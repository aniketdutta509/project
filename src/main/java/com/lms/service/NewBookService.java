package com.lms.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lms.dao.BookRepository;
import com.lms.dao.BookSpecifications;
import com.lms.entities.Book;
import com.lms.exceptions.NotFoundException;

@Service
public class NewBookService {

	private final BookRepository bookRepository;

	public NewBookService(BookRepository bookRepository) {
		this.bookRepository = bookRepository;
	}

	@Transactional
	public List<Book> findAllBooks() {
		return bookRepository.findAll();
	}

	@Transactional
	public List<Book> searchBooks(String publisher, String category, String title) {
		Specification<Book> spec = Specification.where(null);
		if (publisher != null) {
			spec = spec.and(BookSpecifications.hasPublisher(publisher));
		}
		if (category != null) {
			spec = spec.and(BookSpecifications.hasCategory(category));
		}
		if (title != null) {
			spec = spec.and(BookSpecifications.hasTitle(title));
		}
		return bookRepository.findAll(spec);
	}

	@Transactional
	public Book findBookById(Long id) {
		return bookRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(String.format("Book not found with ID %d", id)));
	}
	
	@Transactional
	public void createBook(Book book) {
		bookRepository.save(book);
	}

	@Transactional
	public void deleteBook(Long id) {
		final Book book = bookRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(String.format("Book not found with ID %d", id)));

		bookRepository.deleteById(book.getBookId());
	}

}