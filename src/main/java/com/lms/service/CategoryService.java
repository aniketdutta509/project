package com.lms.service;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lms.dao.BookRepository;
import com.lms.dao.CategoryRepository;
import com.lms.entities.Book;
import com.lms.entities.Category;
import com.lms.exceptions.NotFoundException;

@Service
public class CategoryService {

	private final CategoryRepository categoryRepository;
	private final BookRepository bookRepository;

	public CategoryService(BookRepository bookRepository, CategoryRepository categoryRepository) {
		super();
		this.bookRepository = bookRepository;
		this.categoryRepository = categoryRepository;
	}

	public void createCategory(Category category) {
		categoryRepository.save(category);
	}

//	@Transactional
//	public Category findCategoryByBookId(Long id) {
//
//		return categoryRepository.findById(id)
//				.orElseThrow(() -> new NotFoundException(String.format("Category not found with BOOKID %d", id)));
//	}

	@Transactional
	public String findCategoryByBook(Book book) {
//		return book.getCategory().getCategoryName();
		return categoryRepository.findCategoryByBook(book.getBookId());
	}
}
