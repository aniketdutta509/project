package com.lms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.lms.dao.AdminRepository;
import com.lms.entities.Book;

@Service
public class AdminService {
	
	@Autowired 
	private AdminRepository adminRepo;
	
	public void getAllReturns() {
		System.out.println(adminRepo.findAll());
	}

}
