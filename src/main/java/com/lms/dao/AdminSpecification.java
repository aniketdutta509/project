package com.lms.dao;

import org.springframework.data.jpa.domain.Specification;

import com.lms.entities.Book;
import com.lms.entities.Loans;

import jakarta.persistence.criteria.Join;

public class AdminSpecification {

	public static Specification<Book> hasAuthor(String author) {
		  return (root, query, criteriaBuilder) -> {
		 Join<Book, Loans> bookLoans = root.join("LOANS");
	        return criteriaBuilder.equal(bookLoans, root);
		  };
	
	}
	
}
