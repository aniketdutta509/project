package com.lms.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.lms.entities.Book;

public interface AdminRepository extends JpaRepository<Book,Long>, JpaSpecificationExecutor<Book> {
	

//    @Query("SELECT * FROM BOOKS INNER JOIN LOANS ON BOOKS.BOOK_ID=LOANS.BOOK_ID;" )
//    public String findAllBooks();  
//	
	
}
