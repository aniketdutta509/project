package com.lms.dao;

import org.springframework.data.jpa.domain.Specification;

import com.lms.entities.Book;
import com.lms.entities.Category;

import jakarta.persistence.criteria.Join;

public class BookSpecifications {
	public static Specification<Book> hasPublisher(String publisher) {
		return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder
				.equal(root.get("publisher").get("publisherName"), publisher);
	}

	public static Specification<Book> hasCategory(String category) {
		return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder
				.equal(root.get("category").get("categoryName"), category);
	}

	public static Specification<Book> hasTitle(String title) {
		return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("title"),
				title);
	}
}