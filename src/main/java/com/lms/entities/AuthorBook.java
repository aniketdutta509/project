
package com.lms.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "AUTHOR_BOOK")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AuthorBook implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "AUTHOR_BOOK_ID")
	private int id;

	@ManyToOne
	@JoinColumn(name = "BOOK_ID")
	@JsonIgnore
	private Book book;

	@ManyToOne
	@JoinColumn(name = "AUTHOR_ID")
	@JsonIgnore
	private Author author;

	public AuthorBook(Book b, Author u) {
		this.book = b;
		this.author = u;
	}

	@Override
	public String toString() {
		return "AuthorBook [id=" + id + ", book=" + book + ", author=" + author + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public AuthorBook() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
