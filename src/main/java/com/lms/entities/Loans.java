package com.lms.entities;

import java.time.LocalDate;

import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "LOANS")
public class Loans implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "LOAN_ID")
	private Long loanId;

	@ManyToOne
	@JoinColumn(name = "BOOK_ID")
	private Book book;

	@ManyToOne
	@JoinColumn(name = "MEMBER_ID")
	private User member;

	@Column(name = "LOAN_DATE")
	private LocalDate loanDate;

	@Column(name = "RETURN_DATE")
	private LocalDate returnDate;
	
	public Loans() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Loans(Book book, User member, LocalDate loanDate, LocalDate returnDate) {
		super();
		this.book = book;
		this.member = member;
		this.loanDate = loanDate;
		this.returnDate = returnDate;
	}

	@Override
	public String toString() {
		return "Loans [loanId=" + loanId + ", book=" + book + ", member=" + member + ", loanDate=" + loanDate
				+ ", returnDate=" + returnDate + "]";
	}

	public Long getLoanId() {
		return loanId;
	}

	public void setLoanId(Long loanId) {
		this.loanId = loanId;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public User getMember() {
		return member;
	}

	public void setMember(User member) {
		this.member = member;
	}

	public LocalDate getLoanDate() {
		return loanDate;
	}

	public void setLoanDate(LocalDate loanDate) {
		this.loanDate = loanDate;
	}

	public LocalDate getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(LocalDate returnDate) {
		this.returnDate = returnDate;
	}
}
