package com.lms.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

@Data
@Entity
@Table(name = "USER")
public class User implements java.io.Serializable {

	/**
	 * 
	 */
	

	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	@Column(name = "MEMBER_ID")
	private String id;

	@Column(name = "USERNAME")
	private String username;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "Member_NAME")
	private String mem_name;

	@Column(name = "CONTACT")
	private String phoneNumber;

	@Column(name = "ADDRESS")
	private String address;
	
	@Column(name="ROLE")
	private String role;

	
	

}
