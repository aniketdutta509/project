package com.lms.entities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.*;
import jakarta.validation.Valid;
import lombok.*;

@Entity
@Table(name = "BOOK", indexes = { @Index(name = "IDX_MYIDX1", columnList = "BOOK_ID") })
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Book implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "BOOK_ID", unique = true, nullable = false)
	private long bookId;

	@Column(name = "ISBN", unique = true, nullable = false)
	private String isbn;

	@Column(name = "TITLE")
	private String title;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "PUBLISHER_ID")
	@JsonIgnore
	private Publisher publisher;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "CATEGORY_ID")
	@JsonIgnore
	public Category category;

	@Column(name = "YEAR_OF_PUBLICATION")
	private String yearOfPublication;

	@Column(name = "TOTAL_COPIES")
	private int totalCopies;



	@ManyToMany(mappedBy = "borrowedBooks", cascade = { CascadeType.ALL })
	private Set<User> currentUsers = new HashSet<>();

	@ManyToMany(mappedBy = "books", cascade = { CascadeType.ALL })
	private Set<Author> authors = new HashSet<>();

	public Book(String isbn, String title, Publisher publisher, Category category, String yearOfPublication,
			int totalCopies, boolean available, LocalDate toBeAvailableOn, Set<User> currentUsers,
			Set<Author> authors) {
		super();
		this.isbn = isbn;
		this.title = title;
		this.publisher = publisher;
		this.category = category;
		this.yearOfPublication = yearOfPublication;
		this.totalCopies = totalCopies;
		this.currentUsers = currentUsers;
		this.authors = authors;
	}

	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getBookId() {
		return bookId;
	}

	public void setBookId(long bookId) {
		this.bookId = bookId;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getYearOfPublication() {
		return yearOfPublication;
	}

	public void setYearOfPublication(String yearOfPublication) {
		this.yearOfPublication = yearOfPublication;
	}

	public int getTotalCopies() {
		return totalCopies;
	}

	public void setTotalCopies(int totalCopies) {
		this.totalCopies = totalCopies;
	}



	public Set<User> getCurrentUsers() {
		return currentUsers;
	}

	public void setCurrentUsers(Set<User> currentUsers) {
		this.currentUsers = currentUsers;
	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", isbn=" + isbn + ", title=" + title + ", publisher=" + publisher
				+ ", category=" + category + ", yearOfPublication=" + yearOfPublication + ", totalCopies=" + totalCopies
				+ ", currentUsers=" + currentUsers + ", authors=" + authors + "]";
	}
	
}