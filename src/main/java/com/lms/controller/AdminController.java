package com.lms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lms.service.AdminService;

@Controller
@RequestMapping("/admin")
public class AdminController {

	

	@Autowired
	private AdminService adminServ;
	
	@GetMapping("/")
	public String findAllBooks() {
		
		return "admin_dash";
	}
	
	
	@GetMapping("/getAllReturns")
	private void allReturnsAdmin() {
		adminServ.getAllReturns();
	}
	
}
