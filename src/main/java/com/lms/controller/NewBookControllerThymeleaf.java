package com.lms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lms.entities.*;
import com.lms.service.*;

@CrossOrigin(origins = "null")
@Controller
@RequestMapping("/api")
public class NewBookControllerThymeleaf {

	private final NewBookService newBookService;
	private final CategoryService categoryService;

	@Autowired
	public NewBookControllerThymeleaf(NewBookService newBookService, CategoryService categoryService) {
		super();
		this.newBookService = newBookService;
		this.categoryService = categoryService;
	}

	@RequestMapping("/books")
	public String findAllBooks(Model model) {
		List<Book> books = newBookService.findAllBooks();
		model.addAttribute("books", books);
		return "list-books";
	}

	@GetMapping("/search")
	public String showSearchForm(Book book, Model model) {
		return "search-book";
	}

	@RequestMapping("/search-book")
	public String searchBooks(Book book, Model model) {
		final List<Book> books = newBookService.searchBooks(book.getPublisher().getPublisherName(),
				book.getCategory().getCategoryName(), book.getTitle());
		model.addAttribute("books", books);
		return "search-results";
	}

	@RequestMapping("/book/{id}")
	public String findBookById(@PathVariable("id") Long id, Model model) {
		final Book book = newBookService.findBookById(id);
		final String category = book.getCategory().getCategoryName();
		final String publisher = book.getPublisher().getPublisherName();
		model.addAttribute("book", book);
		model.addAttribute("category", category);
		model.addAttribute("publisher", publisher);
		return "list-book";
	}

	@GetMapping("/add")
	public String showCreateForm(Book book, Model model) {
		return "add-book";
	}

	@RequestMapping("/add-book")
	public String createBook(Book book, Model model) {
		newBookService.createBook(book);
		model.addAttribute("book", newBookService.findAllBooks());
		return "redirect:/api/books";
	}

	@GetMapping("/update/{id}")
	public String showUpdateForm(@PathVariable("id") Long id, Model model) {
		final Book book = newBookService.findBookById(id);
		model.addAttribute("book", book);
		return "update-book";
	}

	@RequestMapping("/update-book/{id}")
	public String updateBook(@PathVariable("id") Long id, Book book, BindingResult result, Model model) {
		if (result.hasErrors()) {
			book.setBookId(id);
			return "update-book";
		}
		newBookService.deleteBook(id);
		newBookService.createBook(book);
		model.addAttribute("book", newBookService.findAllBooks());
		return "redirect:/api/books";
	}

	@RequestMapping("/remove-book/{id}")
	public String deleteBook(@PathVariable("id") Long id, Model model) {
		newBookService.deleteBook(id);
		model.addAttribute("book", newBookService.findAllBooks());
		return "redirect:/api/books";
	}

}
